addpath('C:\Users\student\Desktop\matlabG\jsonlab-1.9.8'); %(tam gdzie jest
%popbrany plik jason)
%help loadjson



%https://danepubliczne.imgw.pl/api/data/synop/format/html/station/olsztyn
%https://wttr.in/olsztyn?format=j1
   

y = urlread('http://wttr.in/opole?format=j1');
x = urlread('https://danepubliczne.imgw.pl/api/data/synop/format/json/station/opole');
y1 = urlread('http://wttr.in/raciborz?format=j1');
x1 = urlread('https://danepubliczne.imgw.pl/api/data/synop/format/json/station/raciborz');
y2 = urlread('http://wttr.in/katowice?format=j1');
x2 = urlread('https://danepubliczne.imgw.pl/api/data/synop/format/json/station/katowice');
stacja = jsondecode(x);  
temp = stacja.temperatura;
cis = stacja.cisnienie;
pred = stacja.predkosc_wiatru;
wilg = stacja.wilgotnosc_wzgledna;
czas = stacja.godzina_pomiaru;
wilg1 = str2num(wilg);
czas1 = str2num(czas);
temp1 = str2num(temp);
cis1 = str2num(cis);
pred1 = str2num(pred);
save('folder2','wilg1','czas1','temp1','cis1','pred1')

stacjax1 = jsondecode(x1);  
tempx1 = stacjax1.temperatura;
cisx1 = stacjax1.cisnienie;
predx1 = stacjax1.predkosc_wiatru;
wilgx1 = stacjax1.wilgotnosc_wzgledna;
czasx1 = stacjax1.godzina_pomiaru;
wilg1x1 = str2num(wilgx1);
czas1x1 = str2num(czasx1);
temp1x1 = str2num(tempx1);
cis1x1 = str2num(cisx1);
pred1x1 = str2num(predx1);
save('folder2','wilg1x1','czas1x1','temp1x1','cis1x1','pred1x1')

stacjax2 = jsondecode(x2);  
tempx2 = stacjax2.temperatura;
cisx2 = stacjax2.cisnienie;
predx2 = stacjax2.predkosc_wiatru;
wilgx2 = stacjax2.wilgotnosc_wzgledna;
czasx2 = stacjax2.godzina_pomiaru;
wilg1x2 = str2num(wilgx2);
czas1x2 = str2num(czasx2);
temp1x2 = str2num(tempx2);
cis1x2 = str2num(cisx2);
pred1x2 = str2num(predx2);
save('folder2','wilg1x1','czas1x1','temp1x1','cis1x1','pred1x1')




stacja2 = jsondecode(y);%w 2011  loadjson(y)
%temp odczuwalna
s = cell2mat((stacja2.current_condition));
wiatr = s.windspeedMiles; %In miles per hour
tem = s.temp_F; %in F
wil = s.humidity; %wilgotnosc wzgeldna w %
wiatr1 = str2num(s.windspeedMiles);
tem1 = str2num(s.temp_F);
wil1 = str2num(s.humidity);
w = cell2mat((stacja2.weather));
wd = w.date;
wh = w.hourly;
wh2 = cell2mat(wh);
wh3 = wh2.time;


stacja2y1 = jsondecode(y1);%w 2011  loadjson(y)
%temp odczuwalna
sy1 = cell2mat((stacja2y1.current_condition));
wiatry1 = sy1.windspeedMiles; %In miles per hour
temy1 = sy1.temp_F; %in F
wily1 = sy1.humidity; %wilgotnosc wzgeldna w %
wiatr1y1 = str2num(sy1.windspeedMiles);
tem1y1 = str2num(sy1.temp_F);
wil1y1 = str2num(sy1.humidity);
wy1 = cell2mat((stacja2y1.weather));
wdy1 = wy1.date;
why1 = wy1.hourly;
wh2y1 = cell2mat(why1);
wh3y1 = wh2y1.time;

stacja2y2 = jsondecode(y2);%w 2011  loadjson(y)
%temp odczuwalna
sy2 = cell2mat((stacja2y2.current_condition));
wiatry2 = sy2.windspeedMiles; %In miles per hour
temy2 = sy2.temp_F; %in F
wily2 = sy2.humidity; %wilgotnosc wzgeldna w %
wiatr1y2 = str2num(sy2.windspeedMiles);
tem1y2 = str2num(sy2.temp_F);
wil1y2 = str2num(sy2.humidity);
wy2 = cell2mat((stacja2y2.weather));
wdy2 = wy2.date;
why2 = wy2.hourly;
wh2y2 = cell2mat(why2);
wh3y2 = wh2y2.time;





if tem1 <= 50 & wiatr1 >= 3
    vFeelsLike = 35.74 + (0.6215*tem1) - 35.75*(wiatr1^0.16) + ((0.4275*tem1)*(wiatr1^0.16))
else
    vFeelsLike = tem1
end
if vFeelsLike == tem1 & tem1 >= 80
    vFeelsLike = 0.5 * (tem1 + 61.0 + ((tem1-68.0)*1.2) + (wil1*0.094))
end
 
if vFeelsLike >= 80
    vFeelsLike = -42.379 + 2.04901523*tem1 + 10.14333127*wil1 - .22475541*tem1*wil1 - .00683783*tem1*tem1 - .05481717*wil1*wil1 + .00122874*tem1*tem1*wil1 + .00085282*tem1*wil1*wil1 - .00000199*tem1*tem1*wil1*wil1
end
if wil1 < 13 & tem1 >= 80 & tem1 <= 112
    vFeelsLike = vFeelsLike - ((13-wil1)/4)*sqrt((17-abs(tem1-95.))/17)
end
if wil1 > 85 & tem1 >= 80 & tem1 <= 87
    vFeelsLike = vFeelsLike + ((wil1-85)/10) * ((87-tem1)/5)
end
vFeelsLikeCelcius = (vFeelsLike-32)/1.8 




if tem1y1 <= 50 & wiatr1y1 >= 3
    vFeelsLikey1 = 35.74 + (0.6215*tem1y1) - 35.75*(wiatr1y1^0.16) + ((0.4275*tem1y1)*(wiatr1y1^0.16))
else
    vFeelsLikey1 = tem1y1
end
if vFeelsLikey1 == tem1y1 & tem1y1 >= 80
    vFeelsLikey1 = 0.5 * (tem1y1 + 61.0 + ((tem1y1-68.0)*1.2) + (wil1y1*0.094))
end
 
if vFeelsLikey1 >= 80
    vFeelsLikey1 = -42.379 + 2.04901523*tem1y1 + 10.14333127*wil1y1 - .22475541*tem1y1*wil1y1 - .00683783*tem1y1*tem1y1 - .05481717*wil1y1*wil1y1 + .00122874*tem1y1*tem1y1*wil1y1 + .00085282*tem1y1*wil1y1*wil1y1 - .00000199*tem1y1*tem1y1*wil1y1*wil1y1
end
if wil1y1 < 13 & tem1y1 >= 80 & tem1y1 <= 112
    vFeelsLikey1 = vFeelsLikey1 - ((13-wil1y1)/4)*sqrt((17-abs(tem1y1-95.))/17)
end
if wil1y1 > 85 & tem1y1 >= 80 & tem1y1 <= 87
    vFeelsLikey1 = vFeelsLikey1 + ((wil1y1-85)/10) * ((87-tem1y1)/5)
end
vFeelsLikeCelciusy1 = (vFeelsLikey1-32)/1.8              

%Taken from  https://gist.github.com/jfcarr/e68593c92c878257550d?fbclid=IwAR1We5QdcSEbG6ohCisCfCa6Syi2aUpyUCBe_jROfZx7z92xnLj5WwCXp1s
%It is a pattern to calculate FeelsLike temperature 


if tem1y2 <= 50 & wiatr1y2 >= 3
    vFeelsLikey2 = 35.74 + (0.6215*tem1y2) - 35.75*(wiatr1y2^0.16) + ((0.4275*tem1y2)*(wiatr1y2^0.16))
else
    vFeelsLikey2 = tem1y2
end
if vFeelsLikey2 == tem1y2 & tem1y2 >= 80
    vFeelsLikey2 = 0.5 * (tem1y2 + 61.0 + ((tem1y2-68.0)*1.2) + (wil1y2*0.094))
end
 
if vFeelsLikey2 >= 80
    vFeelsLikey2 = -42.379 + 2.04901523*tem1y2 + 10.14333127*wil1y2 - .22475541*tem1y2*wil1y2 - .00683783*tem1y2*tem1y2 - .05481717*wil1y2*wil1y2 + .00122874*tem1y2*tem1y2*wil1y2 + .00085282*tem1y2*wil1y2*wil1y2 - .00000199*tem1y2*tem1y2*wil1y2*wil1y2
end
if wil1y2 < 13 & tem1y2 >= 80 & tem1y2 <= 112
    vFeelsLikey2 = vFeelsLikey2 - ((13-wil1y2)/4)*sqrt((17-abs(tem1y2-95.))/17)
end
if wil1y2 > 85 & tem1y2 >= 80 & tem1y2 <= 87
    vFeelsLikey2 = vFeelsLikey2 + ((wil1y1-85)/10) * ((87-tem1y1)/5)
end
vFeelsLikeCelciusy2 = (vFeelsLikey2-32)/1.8;   

fileID = fopen('tekst1.txt','w');
fprintf(fileID, 'DLA STACJI W OPOLU: \n');
fprintf(fileID,'Predkosc wiatru wyra�ona w kilometrach na godzine odczytana z stacji w opolu (danepubliczne) wynosi  %f\n', pred1);
fprintf(fileID,'Wilogotno�� powietrza wyra�ona w procentach odczytana z stacji w opolu (danepubliczne)  wynosi %f\n', wilg1);
fprintf(fileID,'Temperatura wyra�ona w stopniach Celcjusza odczytana z stacji w opolu (danepubliczne)  wynosi %f\n', temp1);
fprintf(fileID,'Czas wyra�ony w godzinie odczytany z stacji w opolu (danepubliczne) wynosi %f\n', czas1);
fprintf(fileID,'Cisnienie wyra�one w hPa odczytana z stacji w opolu (danepubliczne) wynosi %f\n', cis1);

fprintf(fileID,'Predkosc wiatru wyra�ona w milach na godzine odczytana z stacji w opolu (wttr) wynosi  %f\n', wiatr1);
fprintf(fileID,'Temperatura powietrza wyra�ona w Farenhaitach odczytana z stacji w opolu (wttr)  %f\n', tem1);
fprintf(fileID,'Wilogtnosc wyrazona w procentach odczynana z stacji w opolu (wttr) wynosi  %f\n', wil1);
fprintf(fileID,'Temperatura odczuwalna wyra�ona w Celcjuszach odczytana z stacji w opolu (wttr)  %f\n', vFeelsLikeCelcius);
fprintf(fileID,'\n');

fprintf(fileID, 'DLA STACJI W RACIBORZU: \n');
fprintf(fileID,'Predkosc wiatru wyra�ona w kilometrach na godzine odczytana z stacji w raciborzu (danepubliczne) wynosi  %f\n', pred1x1);
fprintf(fileID,'Wilogotno�� powietrza wyra�ona w procentach odczytana z stacji w raciborzu (danepubliczne)  wynosi %f\n', wilg1x1);
fprintf(fileID,'Temperatura wyra�ona w stopniach Celcjusza odczytana z stacji w raciborzu(danepubliczne)  wynosi %f\n', temp1x1);
fprintf(fileID,'Czas wyra�ony w godzinie odczytany z stacji w raciborzu (danepubliczne) wynosi %f\n', czas1x1);
fprintf(fileID,'Cisnienie wyra�one w hPa odczytana z stacji w raciborzu (danepubliczne) wynosi %f\n', cis1x1);

fprintf(fileID,'Predkosc wiatru wyra�ona w milach na godzine odczytana z stacji w raciborzu (wttr) wynosi  %f\n', wiatr1y1);
fprintf(fileID,'Temperatura powietrza wyra�ona w Farenhaitach odczytana z stacji w raciborzu(wttr)  %f\n', tem1y1);
fprintf(fileID,'Wilogtnosc wyrazona w procentach odczynana z stacji w opolu (wttr) raciborzu  %f\n', wil1y1);
fprintf(fileID,'Temperatura odczuwalna wyra�ona w Celcjuszach odczytana z stacji w raciborzu (wttr)  %f\n', vFeelsLikeCelciusy1);
fprintf(fileID,'\n');

fprintf(fileID, 'DLA STACJI W KATOWICACH: \n');
fprintf(fileID,'Predkosc wiatru wyra�ona w kilometrach na godzine odczytana z stacji w katowicach (danepubliczne) wynosi  %f\n', pred1x2);
fprintf(fileID,'Wilogotno�� powietrza wyra�ona w procentach odczytana z stacji w katowicach (danepubliczne)  wynosi %f\n', wilg1x2);
fprintf(fileID,'Temperatura wyra�ona w stopniach Celcjusza odczytana z stacji w katowicach(danepubliczne)  wynosi %f\n', temp1x2);
fprintf(fileID,'Czas wyra�ony w godzinie odczytany z stacji w katowicach (danepubliczne) wynosi %f\n', czas1x2);
fprintf(fileID,'Cisnienie wyra�one w hPa odczytana z stacji w katowicach (danepubliczne) wynosi %f\n', cis1x2);

fprintf(fileID,'Predkosc wiatru wyra�ona w milach na godzine odczytana z stacji w katowicach (wttr) wynosi  %f\n', wiatr1y2);
fprintf(fileID,'Temperatura powietrza wyra�ona w Farenhaitach odczytana z stacji w katowicach(wttr)  %f\n', tem1y2);
fprintf(fileID,'Wilogtnosc wyrazona w procentach odczynana z stacji w opolu (wttr) katowicach  %f\n', wil1y2);
fprintf(fileID,'Temperatura odczuwalna wyra�ona w Celcjuszach odczytana z stacji w katowicach (wttr)  %f\n', vFeelsLikeCelciusy2);



